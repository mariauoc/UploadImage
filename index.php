<!DOCTYPE html>
<!--
 Ejemplo de formulario para subir imagen al servidor
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <!-- Imprescindible indicar enctype en el form -->
        <form action="" method="POST" enctype="multipart/form-data">
            Selecciona el fichero que quieres subir: <input name="fichero_usuario" type="file" />
            <input type="submit" value="Enviar fichero" name="boton"/>
        </form>
        <?php
        if (isset($_POST["boton"])) {
            // Directorio donde queréis guardar la imagen
            $dir = "/var/www/imagenes";
            // Ruta completa de la imagen (podéis guardar eso en la bbdd como url de la imagen
            $fichero_subido = $dir . basename($_FILES['fichero_usuario']['name']);
            // Subimos el fichero al servidor
            if (move_uploaded_file($_FILES['fichero_usuario']['tmp_name'], $fichero_subido)) {
                echo "<p>El fichero es válido y se subió con éxito.</p>";
            } else {
                echo "<p>Ha habido un error al subir el fichero</p>";
            }
        }
        ?>
    </body>
</html>
